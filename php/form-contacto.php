<?php
  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );
  $from = "web@cetecnicolasromero.com.mx";
  $to = "ventasnr@grupocetec.com";
  $subject = 'Mensaje enviado desde el sitio web';
  $message = 'Datos enviados desde el sitio web.' . PHP_EOL .
            PHP_EOL . 'Nombre: ' . $_POST['nombre'] . PHP_EOL . 
            'Telefono: ' . $_POST['telefono'] . PHP_EOL . 
            'Correo electronico: ' . $_POST['correo'] . PHP_EOL .
            'Oferta educativa: ' . $_POST['curso'] . PHP_EOL .
            'Municipio: ' . $_POST['municipio'] . PHP_EOL .
            'Mensaje: ' . $_POST['mensaje'];
  $headers = "From:" . $from;
  print_r($message);
  mail($to,$subject,$message, $headers);
  header("Location: https://cetecnicolasromero.com.mx/contacto.html");
  die();
?>